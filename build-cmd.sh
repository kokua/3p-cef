#!/bin/bash

URL_BASE=http://192.168.1.115/dev/pkg

CEF_LINUX64=cef_binary_3.3626.1895.g7001d56_linux64_minimal.tar.bz2
CEF_LINUX64_MD5=fccd572b46d09b9b04f22d74c189ca48
CEF_LINUX64_DIR=`basename $CEF_LINUX64 .tar.bz2`

cd "$(dirname "$0")"

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

# Check autobuild is around or fail
if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

stage="$(pwd)/stage"

if [ -d "${stage}" ]
then
 rm -rf "${stage}/lib"
 rm -rf "${stage}/include"
fi

mkdir -p "$stage/lib/debug"
mkdir -p "$stage/lib/release"

if [ ! -z $ND_CLEAN_BEFORE_BUILD ]
then
    rm -rf "build-${AUTOBUILD_PLATFORM}-${AUTOBUILD_ADDRSIZE}"
    mkdir -p "build-${AUTOBUILD_PLATFORM}-${AUTOBUILD_ADDRSIZE}"
fi

if [ ! -d "build-${AUTOBUILD_PLATFORM}-${AUTOBUILD_ADDRSIZE}" ]
then
    mkdir -p "build-${AUTOBUILD_PLATFORM}-${AUTOBUILD_ADDRSIZE}"
fi
    
mkdir -p "$stage/LICENSES"
mkdir -p "$stage/include"
mkdir -p "$stage/lib/release"
mkdir -p "$stage/lib/debug"
mkdir -p "$stage/resources"

pushd "build-${AUTOBUILD_PLATFORM}-${AUTOBUILD_ADDRSIZE}"
    case "$AUTOBUILD_PLATFORM" in
        linux64)
            CEF_LINUX_DIR=$CEF_LINUX64_DIR

			if [ ! -f ${CEF_LINUX64} ]
			then
				curl -O ${URL_BASE}/${CEF_LINUX64}
			fi
			
			echo "${CEF_LINUX64_MD5} ${CEF_LINUX64}" | md5sum -c -
			
            CMAKE_CXX_FLAGS=-m64
            
            tar xf $CEF_LINUX64
            cd $CEF_LINUX_DIR

            mkdir -p build
            cd build
            cmake .. -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS} || true
            make -j6 libcef_dll_wrapper
            cd ../..


            cp $CEF_LINUX_DIR/build/libcef_dll_wrapper/libcef_dll_wrapper.a ${stage}/lib/release/
            cp -R $CEF_LINUX_DIR/Release/* ${stage}/lib/release/
            cp -R $CEF_LINUX_DIR/Resources/* ${stage}/resources/
                
            cp -R $CEF_LINUX_DIR/include/* ${stage}/include/

            sudo chown root:root ${stage}/lib/release/chrome-sandbox
            sudo chmod 4755 ${stage}/lib/release/chrome-sandbox

            gawk '/ CEF_VERSION ".*"/ { gsub("\"","",$3) ; print $3}' <$CEF_LINUX_DIR/include/cef_version.h > ${stage}/version.txt
        ;;
    esac
popd

cp LICENSES/* "$stage/LICENSES"

